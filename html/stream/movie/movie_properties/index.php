<?php include_once('/var/www/inc/.php');


if(! $movie_id = intval( req::get('movie_id') ) ){
    json::die([ 'status'=>'ER', 'code'=>'no parameter defined' ]);

} else {

    $query = " SELECT `stream_display_name` movie_name, `movie_propeties` FROM `streams` WHERE `id`={$movie_id} LIMIT 1 ";

    if(! $rs = dbq($query) ){
        json::die([ 'status' => 'ER', 'code' => dbe() ]);

    } else if( !dbn($rs) or !$rw = dbf($rs) ){
        json::die([ 'status' => 'ER', 'code' => 'user not found with id '.$user_id ]);

    } else if( json::is($rw['movie_propeties']) ){	    
        $movie_propeties = json_decode($rw['movie_propeties'], true);

        if(! $movie_propeties['name'] ){
            $movie_propeties['name'] = strstr($rw['movie_name'], ": ")
                ? substr( strstr($rw['movie_name'], ": "), 2 )
                : $rw['movie_name']
                ;
        }

        json::die([ 'status' => 'OK', 'code' => $movie_propeties ]);
    }

}

