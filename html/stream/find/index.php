<?php include_once('/var/www/inc/.php');


if( !is_array($_POST) or !sizeof($_POST) )
    json::die([ 'status'=>'ER', 'code'=>'no parameter defined' ]);

$query = queryBuilder::find('streams', $_POST);


if(! $rs = dbq($query) ){
    json::die([ 'status'=>'ER', 'code'=>dbe() ]);

} else {

    $rw_s = [];

    while( $rw = dbf($rs) ){
        $rw_s[] = $rw;
    }

    json::die([ 'status'=>'OK', 'code'=>$rw_s ]);

}



