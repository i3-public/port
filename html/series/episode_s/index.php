<?php include_once('/var/www/inc/.php');


if(! $series_id = req::get('series_id') )
    json::die([ 'status'=>'ER', 'code'=>'no series_id defined' ]);

$query = " SELECT `series_episodes`.*, `streams`.`stream_display_name` AS title, `streams`.`target_container`, `streams`.`added`, `streams`.`movie_propeties` info
FROM `series_episodes`
LEFT JOIN `streams` ON `series_episodes`.`stream_id` = `streams`.`id`
WHERE `series_id`={$series_id} ";

if(! $rs = dbq($query) ){
    json::die([ 'status'=>'ER', 'code'=>dbe() ]);

} else {

    $rw_s = [];

    while( $rw = dbf($rs) ){
        $rw_s[] = $rw;
    }

    json::die([ 'status'=>'OK', 'code'=>$rw_s ]);

}



