<?php include_once('/var/www/inc/.php');


if(! $user_id_s = $_POST['user_id_s'] ){ // 11,22,33,44
    json::die([ 'status' => 'ER', 'code' => ' no user_id_s defined' ]);

} else if(! $rs = dbq(" SELECT `users`.`id`, COUNT(*) as cur, `users`.`max_connections` max FROM `user_activity_now` LEFT JOIN `users` ON `users`.`id`=`user_activity_now`.`user_id` WHERE `max_connections`!=0 AND `users`.`id` IN ($user_id_s) GROUP BY `user_activity_now`.`user_id` ") ){
    json::die([ 'status' => 'ER', 'code' => dbe() ]);

} else {

    $data = [];
    $over = [];

    while( $rw = dbf($rs) ){

        extract($rw);
        
        if( $cur >= $max ){
            
            $data[] = $id; // list of over users

            if( $cur > $max ){
                $over[] = $id;
            }

        }

    }

    if( sizeof($over) ){
        $over_str = implode(',', $over);
        dbq(" UPDATE `users` SET `overuse_state`=UNIX_TIMESTAMP() WHERE `id` IN ($over_str) ");
    }
	    
    json::die([ 'status'=>'OK', 'code'=> $data ]);

}


