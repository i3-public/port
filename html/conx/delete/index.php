<?php include_once('/var/www/inc/.php');
// https://gitlab.com/i3-public/lb3/-/blob/main/cron/sync_conn.php?ref_type=heads


$query = queryBuilder::delete('user_activity_now', $_POST);

if(! $rs = dbq($query) ){
    json::die([ 'status'=>'ER', 'code'=>dbe() ]);

} else {
    json::die([ 'status'=>'OK', 'code'=>dbaf() ]);
}
