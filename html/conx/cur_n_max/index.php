<?php include_once('/var/www/inc/.php');


if( !array_key_exists('user_id', $_POST) or !$user_id = intval($_POST['user_id']) ){
    json::die([ 'status' => 'ER', 'code' => ' no user_id defined' ]);

} else {
    
    $q_ignore = '';
    if( array_key_exists('ignore', $_POST) and $ignore = $_POST['ignore'] ){
        $q_ignore = " AND `user_activity_now`.`stream_id` NOT IN ($ignore) ";
    }

    // $query = " SELECT `users`.`id`, COUNT(`user_activity_now`.`user_id`) AS cur, `users`.`max_connections` max FROM `users` LEFT JOIN `user_activity_now` ON `user_activity_now`.`user_id`=`users`.`id` WHERE `users`.`id`=$user_id $q_ignore GROUP BY `user_activity_now`.`user_id` ";
    
    $query = " SELECT `users`.`id`, (SELECT COUNT(*) FROM `user_activity_now` WHERE `user_activity_now`.`user_id`=`users`.`id` $q_ignore ) AS cur, `users`.`max_connections` max FROM `users` WHERE `id`=$user_id ";

    if(! $rs = dbq($query) ){
        json::die([ 'status' => 'ER', 'code' => dbe() ]);

    } else if( !dbn($rs) or !$rw = dbf($rs) ){
        json::die([ 'status' => 'ER', 'code' => 'user not found with id '.$user_id ]);

    } else {	    
        json::die([ 'status' => 'OK', 'code' => $rw ]);
    }

}


