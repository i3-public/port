<?php include_once('/var/www/inc/.php');


if(! req::post(['server_name', 'code']) ){
    json::die(['code'=>'ER', 'msg'=>'required variables not defined.']);

} else {

    $server_name = $_POST['server_name'];
    $code = $_POST['code'];

    $res = net_usage_remote_exec($server_name, $code);
    json::die(['status'=>'OK', 'res'=>$res]);

}

