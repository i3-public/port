<?php include_once('/var/www/inc/.php');
// https://gitlab.com/i3-public/lb3/-/blob/main/cron/sync_livestreams.php?ref_type=heads
// https://gitlab.com/i3-public/lb3/-/blob/main/live_assist.php?ref_type=heads
// https://gitlab.com/i3-public/lb3/-/blob/main/html/sys.php?ref_type=heads


$server_id = familyFence::lb_id(' `id` ');

if(! $stream_id = intval($_POST['stream_id']) ){
    json::die(['status'=>'ER', 'code'=>'wrong parameters: '.json_encode($_POST)]);

} else {

    $q_str = [];
    
    if( isset($_POST['stream_status']) )
        $q_str[] = "`stream_status`=".intval($_POST['stream_status']);
    
    if( isset($_POST['stream_started']) ){
        $q_str[] = "`stream_started`=".gmdate('U');
        if(! isset($_POST['this_try_time']) )
            if( isset($_POST['pid']) and is_numeric($_POST['pid']) and $_POST['pid'] > 0 )
                $q_str[] = "`this_try_time`=0";
    }
    
    if( isset($_POST['this_try_time']) )
        $q_str[] = "`this_try_time`=".gmdate('U');
    
    if( isset($_POST['pid']) )
        $q_str[] = "`pid`=" . ( ($_POST['pid'] === 'NULL') ? 'NULL' : intval($_POST['pid']) );
    
    if( isset($_POST['current_source']) ){
        $_POST['current_source'] = trim($_POST['current_source']);
        $q_str[] = "`current_source`=".( ($_POST['current_source'] == 'NULL') ? "NULL" : "'".trim($_POST['current_source'])."'" );
    }

    $q_str = implode(', ', $q_str);

    if( $q_str == '' )
        json::die(['status'=>'ER', 'code'=>'no parameter to sync']);

    if( isset($_POST['stream_started']) ){
        
        $rs = dbq(" UPDATE `streams_sys` 
            SET `last_started`=`stream_started` 
            WHERE `stream_id`=$stream_id AND `server_id`=$server_id 
            LIMIT 1 ");

        if(! $rs )
            json::die(['status'=>'ER', 'code'=>'issue in database query execution -- '.__LINE__]);

    }

    if( isset($_POST['this_try_time']) ){
        
        $rs = dbq(" UPDATE `streams_sys` 
            SET `last_try_time`=`this_try_time` 
            WHERE `stream_id`=$stream_id AND `server_id`=$server_id 
            LIMIT 1 ");

        if(! $rs )
            json::die(['status'=>'ER', 'code'=>'issue in database query execution -- '.__LINE__]);

    }

    $rs = dbq(" UPDATE `streams_sys` 
        SET $q_str
        WHERE `stream_id`=$stream_id AND `server_id`=$server_id 
        LIMIT 1 ");

    if(! $rs )
        json::die(['status'=>'ER', 'code'=>'issue in database query execution -- '." UPDATE `streams_sys` 
        SET $q_str
        WHERE `stream_id`=$stream_id AND `server_id`=$server_id 
        LIMIT 1 "]);

    else
        json::die(['status'=>'OK']);

}


// }



// # `streams_sys`.`monitor_pid`, 
// } else if(! $rs = dbq(" SELECT `streams_sys`.`pid`, `streams_sys`.`stream_id`, `streams_sys`.`current_source`, `streams`.`stream_source` FROM `streams_sys` LEFT JOIN `streams` ON `streams_sys`.`stream_id`=`streams`.`id` WHERE `server_id`=$server_id ") ){
//     json::die(['status'=>'ER', 'code'=>'cant fetch list of streams']);

// } else {
    
//     while( $rw = dbf($rs) ){
//         // if(! $rw['current_source'] )
//         //     unset($rw['current_source']);
//         $rw['stream_source'] = json_decode($rw['stream_source'], true);
//         $items[] = $rw;
//     }

//     json::die(['status'=>'OK', 'items'=>$items]);

// }