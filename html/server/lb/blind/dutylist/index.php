<?php include_once('/var/www/inc/.php');
// https://gitlab.com/i3-public/lb3/-/blob/main/cron/sync_livestreams.php?ref_type=heads
// https://gitlab.com/i3-public/lb3/-/blob/main/live_assist.php?ref_type=heads
// https://gitlab.com/i3-public/lb3/-/blob/main/html/sys.php?ref_type=heads


$server_id = familyFence::lb_id(' `id` ');
$q_etc = '';

if( isset($_GET['stream_id']) and $stream_id = intval($_GET['stream_id']) )
    $q_etc.= " AND `stream_id`=$stream_id ";

$query = " SELECT `streams_sys`.`pid`, `streams_sys`.`stream_started`, `streams_sys`.`last_started`, `streams_sys`.`stream_id`, `streams_sys`.`current_source`, `streams`.`stream_source`, `streams_sys`.`last_try_time`, `streams_sys`.`this_try_time` FROM `streams_sys` LEFT JOIN `streams` ON `streams_sys`.`stream_id`=`streams`.`id` WHERE `server_id`=$server_id $q_etc ";

// log::it($query);

if(! $rs = dbq($query) ){
    json::die(['status'=>'ER', 'code'=>'cant fetch list of streams']);

} else {
    
    while( $rw = dbf($rs) ){
        // if(! $rw['current_source'] )
        //     unset($rw['current_source']);
        $rw['stream_source'] = json_decode($rw['stream_source'], true);
        $items[] = $rw;
    }

    json::die(['status'=>'OK', 'code'=>$items]);

}

