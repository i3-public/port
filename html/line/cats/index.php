<?php include_once('/var/www/inc/.php');


$cat_s = [];
$ignore_subcat = req::get('ignore_subcat') == 1;

$username = req::get('username');
$password = req::get('password');

if( $mac = req::get('mac') ){

    $mac = urldecode($mac);
    $mac = trim($mac);
    $mac = strtoupper($mac);
    $mac_enc = base64_encode($mac);

    $rs = dbq(" SELECT `username`, `password` FROM `users` WHERE `id` IN (SELECT `user_id` FROM `mag_devices` WHERE `mac`='$mac_enc') LIMIT 1 ");
    if( $rw = dbf($rs) ){
        $username = $rw['username'];
        $password = $rw['password'];
    }

}

if(! $username or !$password ){
    echo 'no credentials defined';

} else if(! $rs = dbq(" SELECT `bouquet` FROM `users` WHERE `username`='$username' AND `password`='$password' LIMIT 1 ") ){
    echo dbe();

} else if(! $bq = dbr($rs, 0, 0) ){
    echo "no client found";

} else {

    if(! in_array($bq,['', '[]', '[""]']) ){
        
        $bq_s = json_decode($bq, true);
        $bq_str = implode(',', $bq_s);
        $rs = dbq(" SELECT DISTINCT `prefix` FROM `bouquets` WHERE `id` IN ($bq_str) ");

        $prefix_s = [];

        while( $rw = dbf($rs) ){

            $prefix = ( substr($rw['prefix'], -4) == ' VIP' )
                ? explode(' VIP', $rw['prefix'])[0]
                : $rw['prefix'];

                if(! in_array($prefix, $prefix_s) )
                $prefix_s[] = $prefix;

        }

        if( sizeof($prefix_s) ){

            if( isset($_GET['type']) and $type = $_GET['type'] and in_array($type, ['live', 'movie', 'series']) ){
                $q_type = " AND `category_type`='$type' ";
            
            } else {
                $q_type = '';
            }
            
            $prefix_str = '"'.implode('","', $prefix_s).'"';
            
            $rs = dbq(" SELECT `id`, `category_name`, `parent_id` FROM `stream_categories` WHERE 1
                $q_type
                AND (
                    (`category_type`!='series' AND `id` IN (SELECT DISTINCT `category_id` FROM `streams` WHERE `category_id` IS NOT NULL))
                    OR 
                    (`category_type`='series' AND `id` IN (SELECT DISTINCT `category_id` FROM `series`))
                )
                AND ( 0
                    OR `prefix` IN ($prefix_str) 
                    ".( $ignore_subcat ? '' : "OR `parent_id` IN (SELECT `id` FROM `stream_categories` WHERE `prefix` IN ($prefix_str) )" )."
                )
                ORDER BY `cat_order` ASC, `id` ASC ");
                
            if( dbn($rs) )
                while( $rw = dbf($rs) )
                    $cat_s[] = [ 'category_id'=>$rw['id'], 'category_name'=>$rw['category_name'], 'parent_id'=>$rw['parent_id'] ];

        }
    }
}

header('Content-Type: application/json');
header("Pragma: no-cache");
header("Expires: 0");


echo sizeof($cat_s)
    ? json_encode($cat_s)
    : '[{}]';

die;

