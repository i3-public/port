<?php include_once('/var/www/inc/.php');
// https://gitlab.com/i3-public/lb3/-/blob/main/cron/sync_allowed_ips.php?ref_type=heads

$arr = [];

if(! $rs = dbq(" SELECT `username`, `allowed_ips` FROM `users` WHERE `not_paid_new`=0 AND `enabled`=1 AND `admin_enabled`=1 AND (`exp_date` IS NULL or `exp_date` > UNIX_TIMESTAMP() ) AND `allowed_ips` NOT IN ('', '[]') ") ){
    json::die(['status'=>'ER', 'code'=>'database error']);

} else if(! dbn($rs) ){
    json::die(['status'=>'ER', 'code'=>'no item found in database']);

} else {

    while( $rw = dbf($rs) ){
        extract($rw);
        $arr[ $username ] = json_decode($allowed_ips, true);
    }

    json::die([ 'status'=>'OK', 'code'=>$arr ]);

}


