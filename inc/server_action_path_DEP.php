<?php


function server_action_path( $ip_port ){

	$info = list_of_servers(['main', 'lb'])[ $ip_port ];

	if( $info['action_port'] ){
		list($ip, $port) = explode(':', $ip_port);
		$port = $info['action_port'];
		$ip_port = $ip.":".$port;
	}

	return 'http://'.$ip_port.'/net-usage/agent.php';

}



