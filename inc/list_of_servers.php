<?php


function list_of_servers( $type=null ){

	if( $type and is_array($type) ){
		$type = implode(',', $type);
	}
	
	$json = net::wget('https://xwork.app/api/feed/etc/server_list/'.( $type ? "?type={$type}" : '' ) );
	$list = json_decode($json, true);

	return $list;

}


