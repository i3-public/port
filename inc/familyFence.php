<?php


class familyFence {


    public static function guard(){

        if( req::get('test') == 1 ){
            echo "<pre>";
            echo $_SERVER['SERVER_ADDR'];
            echo "\n";
            echo net::remote_ip();
            die;
        }

        // return true;
        $list = net::wget('https://xwork.app/api/feed/etc/server_list/?only_ip=1');
        $list = json_decode($list, true);

        if(! in_array( net::remote_ip() , $list) )
            die('nil.');
        
    }


    public static function lb_id( $column='*' ){

        $column = trim($column);
        $remote_ip = net::remote_ip();

        $query = " SELECT {$column} FROM `streaming_servers` WHERE `server_ip`='$remote_ip' LIMIT 1 ";

        if(! $rs = dbq($query) ){
            json::die(['status'=>'ER', 'code'=>'database error']);
        
        } else if(! dbn($rs) ){
            json::die(['status'=>'ER', 'code'=>'cant find the server '.$remote_ip]);

        } else if(! $rw = dbf($rs) ){
            json::die(['status'=>'ER', 'code'=>'cant fetch the data for '.$remote_ip]);
        
        } else {
            
            return ( !strstr($column, ',') and !strstr($column, '*') )
                ? reset($rw)
                : $rw
                ;

        }    
        
    }


}