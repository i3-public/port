<?php

ini_set('display_errors', 'On');
error_reporting(E_ALL & ~E_NOTICE);
$reserved_files = [ 'dotenv' ];

foreach( $reserved_files as $file ){
	$file = '/var/www/inc/'.$file.'.php';
	if( file_exists($file) )
		include_once($file);
}

foreach( glob('/var/www/inc/*.php') as $file )
	if(! in_array($file, get_included_files()) )
		include_once($file);

global $dp;
if(! $dp = mysqli_connect( MYSQL_HOST.":7999", MYSQL_USER, MYSQL_PASS, 'xtream_iptvpro' ) )
	die("Can't connect to mysql server of main server");

mysqli_query($dp, " SET NAMES utf8 ");


familyFence::guard();

