<?php


class queryBuilder {


    # 
    # find
    public static function find( $table, $_VAR=[] ){
    
        # need
        # 
        $q_need = '*';
        if( isset($_VAR['need']) and $_VAR['need'] != '' )
            $q_need = trim($_VAR['need']);
        #
        #
    
        # where
        # 
        $WHERE = self::where($_VAR);
        # 
        # 
    
        # group
        # 
        $q_group = '';
        if( isset($_VAR['group']) and $_VAR['group'] != '' )
            $q_group = 'GROUP BY '.trim($_VAR['group']);
        # 
        # 
    
        # order
        # 
        $q_order = '';
        if( isset($_VAR['order']) and $_VAR['order'] != '' )
            $q_order = 'ORDER BY '.trim($_VAR['order']);
        # 
        # 
    
        # limit
        # 
        $LIMIT = self::limit($_VAR);
        # 
        # 
    
        # query
        # 
        $query = " SELECT $q_need FROM `$table` $WHERE $q_group $q_order $LIMIT ";
        
        return $query;
    
    }


    # 
    # insert
    public static function insert( $table, $_VAR=[] ){
        
        if( array_key_exists('column', $_VAR) and array_key_exists('values', $_VAR) ){
            return self::insert_modern($table, $_VAR['column'], $_VAR['values']);
        
        } else {
            return self::insert_legacy($table, $_VAR);
        }

    }

    private static function insert_modern( $table, $column, $values ){

        # 
        # column
        $col_s = self::string_to_column($column);
        #

        #
        # val
        foreach( $values as $j => $value ){
            foreach( $value as $i => $val ){
                if( $val == 'NULL' ){
                    //
                } else if( is_numeric($val) ){
                    // 
                } else {
                    $value[$i] = "'{$val}'";
                }
            }
            $val_s[] = '(' . implode(", ", $value) . ')';
        }
        $val_s = implode(', ', $val_s);
        #

        # 
        # query
        $query = " INSERT INTO `$table` $col_s VALUES $val_s ";
        return $query;
        # 

    }

    private static function insert_legacy( $table, $_VAR=[] ){

        $col_s = $val_s = [];

        foreach( $_VAR as $key => $val ){
            $col_s[] = $key;
            $val_s[] = $val;
        }

        if(! sizeof($col_s) or !sizeof($val_s) or sizeof($col_s) != sizeof($val_s) ){
            echo "ER ".__LINE__;
            return false;

        } else {

            $col_s = '`' . implode('`, `', $col_s) . '`' ;

            foreach( $val_s as $i => $val ){
                $val_s[$i] = self::value_doublecot($val);
            }
            $val_s = implode(', ', $val_s);

            $query = " INSERT INTO `$table` ($col_s) VALUES ($val_s) ";
            // log::it($query);
            return $query;
        }

    }


    private static function string_to_column( $column ){

        #
        # col
        $column = trim($column);
        $col_s = explode(' ', $column);
        foreach( $col_s as $i => $col ){
            $col_s[$i] = trim($col_s[$i]);
        }
        $col_s = '(`' . implode('`, `', $col_s) . '`)';
        #

        return $col_s;

    }


    private static function value_doublecot( $val ){
        
        if( $val == 'NULL' ){
            //
        } else if( is_numeric($val) ){
            // 
        } else {
            $val = "'{$val}'";
        }

        return $val;

    }
    

    # 
    # delete
    public static function delete( $table, $_VAR=[] ){

        # where
        # 
        $WHERE = self::where($_VAR);
        # 

        # limit
        # 
        $LIMIT = self::limit($_VAR);
        # 
        # 

        # query
        # 
        $query = " DELETE FROM `$table` $WHERE $LIMIT ";
        // log::it($query);
        return $query;
        # 

    }


    private static function where( $_VAR ){

        if( array_key_exists('column', $_VAR) and array_key_exists('values', $_VAR) ){
            return self::where_modern($_VAR['column'], $_VAR['values']);
        
        } else if( array_key_exists('filter', $_VAR) ){
            return self::where_legacy($_VAR);
        } 

    }
    
    private static function where_legacy( $_VAR ){

        if( isset($_VAR['filter']) and sizeof($_VAR['filter']) ){

            $q_filter = ' WHERE 1';

            foreach( $_VAR['filter'] as $filter_key => $filter_val ){
    
                if( is_array($filter_val) ){
                    
                    list($operator, $filter_val) = $filter_val;
                    $operator = trim($operator);

                    $q_filter.= " AND `$filter_key`";
    
                    switch ( strtolower($operator) ) {
    
                        case 'not':
                            $q_filter.= "!=". ( is_numeric($filter_val) ? $filter_val : "'{$filter_val}'" );
                            break;
    
                        case 'in': 
                            if( is_array($filter_val) ){
                                if( array_key_exists(0, $filter_val) ){
                                    $filter_val = is_numeric($filter_val[0]) 
                                        ? implode(', ', $filter_val)
                                        : "'" . implode("','", $filter_val) . "'";
                                    $q_filter.= " IN ($filter_val) ";
                                }
                            }
                            break;
    
                        case 'not in': 
                            if( is_array($filter_val) ){
                                if( array_key_exists(0, $filter_val) ){
                                    $filter_val = is_numeric($filter_val[0]) 
                                        ? implode(', ', $filter_val)
                                        : "'" . implode("','", $filter_val) . "'";
                                    $q_filter.= " NOT IN ($filter_val) ";
                                }
                            }
                            break;
                        
                        case 'like':
                            $q_filter.= " LIKE '%{$filter_val}%'  ";
                            break;
    
                        case 'not like':
                            $q_filter.= " NOT LIKE '%{$filter_val}%' ";
                            break;
                                
                    }
    
                } else {
                    $q_filter.= " AND `$filter_key`=". ( is_numeric($filter_val) ? $filter_val : "'{$filter_val}'" ) ;
                }
            }

            unset($_VAR['filter']);
        
        } else {
            $q_filter = '';
        }

        return $q_filter;

    }

    private static function where_modern( $column, $values ){

        if( !is_array($values) or !sizeof($values) )
            return " WHERE 0 ";

        $column = trim($column);
        $column = explode(' ', $column);

        $q_filter = ' WHERE 1 ';

        foreach( $values as $value ){
            foreach( $value as $i => $val ){
                $q_and[] = ' `'.$column[$i].'`='.self::value_doublecot($val).' ';
            }
            $q_or[] = " ( " . implode(' AND ', $q_and) . " ) ";
            $q_and = [];
        }

        $q_filter.= " AND ( " . implode(' OR ', $q_or) . " ) ";
        $q_or = [];
        
        return $q_filter;

    }



    private static function limit( $_VAR ){
        
        $q_limit = '';
        if( isset($_VAR['limit']) and $_VAR['limit'] != '' )
            $q_limit = 'LIMIT '.trim($_VAR['limit']);

        return $q_limit;

    }

}

