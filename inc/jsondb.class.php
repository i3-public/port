<?php


class jsondb {


    private static string $PATH = '/tmp/jsondb/';


    public static function update_or_insert( $db, $arr ){

        // find it

        // if found, update, save
        // if not found insert

    }

    public static function insert( $db, $arr ){
        
        proc::func_semaphore_open(__CLASS__);

        list($status, $data) = self::find($db);
        $id = sizeof($data) ? end($data)['id'] + 1 : 1000;
        $data[] = array_merge(['id'=>$id], $arr);
        self::save($db, $data);
        
        proc::func_semaphore_close(__CLASS__);
        return ['OK', ''];

    }

    # filter -> [ 'key' => 11 ] one record
    public static function find( $db, $filter=null ){
        $path = self::dbpath($db);
        if( file_exists($path) ){
            $json = file_get_contents($path);
            $data = json_decode($json, true);    
        } else {
            $data = [];
        }

        // if( $filter ){
            
        //     $k = array_key_first($filter);
        //     $v = array_key_first
        //     foreach( $data as $i => $r ){
        //         if( in_array() )
        //     }
        // }

        return ['OK', $data];
    }


    public static function delete( $db, $id ){
        
        proc::func_semaphore_open(__CLASS__);
        
        list($status, $data) = self::find($db);
        foreach( $data as $i => $r ){
            if( $r['id'] == $id ){
                unset($data[ $i ]);
                break;
            }
        }
        self::save($db, $data);
        
        proc::func_semaphore_close(__CLASS__);
        return ['OK', ''];

    }


    private static function save( $db, $data ){
        if(! file_exists(self::$PATH) )
            mkdir(self::$PATH);
        $path = self::dbpath($db);
        $json = json_encode($data, JSON_PRETTY_PRINT);
        file_put_contents($path, $json);
    }

    
    private static function dbpath( $db ){
        return self::$PATH.$db.".json";
    }


}


