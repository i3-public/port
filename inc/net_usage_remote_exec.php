<?php

# server_name -> name or ip <[]>

function net_usage_remote_exec( $server_name, $code ){

    if( is_array($server_name) ){
        $res = [];
        foreach( $server_name as $server_this )
            $res[$server_this] = net_usage_remote_exec($server_this, $code);
        return $res;
    }

    // return $server_name." -> ".$code;

    $server_s = list_of_servers();

    if( net::is_ip($server_name) ){
        $its = 'ip';
    } else {
        $its = 'name';
    }

    foreach( $server_s as $server_type => $item_s ){
        foreach( $item_s as $ip_port => $name ){
            list($ip, $port) = explode(':', $ip_port);
            $ip_port = $ip.":9320";
            if( 
                ( $its == 'name' and $server_name == $name )
                or
                ( $its == 'ip' and $server_name == $ip )
            ){
                $url = 'http://'.$ip_port.'/net-usage/agent.php';
                break 2;
            }
        }
    }

    $res = net::wget( $url.'?do=handshake', [ 'timeout' => 2 ]);
    trim($res, "\r\n\t ");

    if( $res != 'OK' ){
        return ['status'=>'ER', 'res'=>'Can\'t access the server'];

    } else {

        $code = str_replace("\r\n", "\n", $code);
        $code = base64_encode($code);
        $text = net::wget( $url, [ 'post' => [ 'token'=>timedhash_generate_now('m'), 'code'=>$code ] ]);

        if( substr($text, 0, 5) == '#done' )
            return ['status'=>'OK', 'res'=>trim(substr($text, 5), "\r\n\t ")];
        
        else 
            return ['status'=>'ER', 'res'=>'wrong result: '.$text];

    }

    return ['status'=>'ER', 'res'=>'null'];

}
