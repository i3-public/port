<?php


class cache {

    private static string $CACHE_DIR = '/tmp/cache';

    public static function keygen( $ITEMS ){

        if( is_array($ITEMS) )
            $ITEMS = json_encode($ITEMS);

        $KEY = md5($ITEMS);
        return $KEY;

    }

    public static function store( $CACHE_KEY, $DATA ){

        $PATH = self::filepath($CACHE_KEY);
        self::care_path($PATH);
    
        $JSON = json_encode($DATA);
        file_put_contents($PATH, $JSON);

        if(! file_exists($PATH) ){
            return ['ER', 'Cant store the file'];

        } else {
            return ['OK', '' ];
        }

    }

    public static function release( $CACHE_KEY, $CACHE_TIME ){
        
        $PATH = self::filepath($CACHE_KEY);
        
        if(! file_exists($PATH) ){
            return ['ER', 'does not exists'];
        
        } else if( filemtime($PATH) < date('U') - $CACHE_TIME ){
            return ['ER', 'cache file expired'];

        } else {
            
            $JSON = file_get_contents($PATH);
            $DATA = json_decode($JSON, true);
            
            return ['OK', $DATA, filemtime($PATH) ];

        }

    }

    private static function filepath( $CACHE_KEY ){
        
        $DIR0 = substr($CACHE_KEY, 0, 4);
        $DIR1 = substr($CACHE_KEY, -4);
        $PATH = self::$CACHE_DIR."/{$DIR0}/{$DIR1}/{$CACHE_KEY}.cache";
        
        return $PATH;

    }

    private static function care_path( $PATH ){

        self::care_path_subdir(self::$CACHE_DIR);

        $PATH_Dir_s = substr( $PATH, strlen(self::$CACHE_DIR) +1 );
        list($DIR0, $DIR1, ) = explode("/", $PATH_Dir_s);

        self::care_path_subdir(self::$CACHE_DIR."/".$DIR0);
        self::care_path_subdir(self::$CACHE_DIR."/".$DIR0."/".$DIR1);

    }

    private static function care_path_subdir( $SUB ){
        if(! file_exists($SUB) ){
            shell_exec(" sudo mkdir {$SUB} ");
            shell_exec(" sudo chown -R www-data:www-data {$SUB} ");
        }
    }
        

}


