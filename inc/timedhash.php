<?php

// 14 Mar 2020

// generates a code related to $code, with a valid time (in hours)
// it makes you sure that we are making the request, not any body else
// $duration [ day, hour, minute, second ]


function timedhash_generate( $duration, $code='' ){

	list( $timeform, $next ) = timedhash_duration($duration);

	$now = gmdate('Y-m-d '.$timeform, gmdate('U') );
	$ohl = gmdate('Y-m-d '.$timeform, gmdate('U') + $next );

	return timedhash_encode($now, $code) . timedhash_encode($ohl, $code);

}


function timedhash_generate_now( $duration, $code='' ){

	list( $timeform, $next ) = timedhash_duration($duration);
	$now = gmdate('Y-m-d '.$timeform, gmdate('U') );

	return timedhash_encode($now, $code);

}


function timedhash_duration( $duration ){

	switch( strtolower($duration) ){

		case 's':
		case 'sec':
		case 'second': return [ 'H:i:s', 1 ];

		case 'm':
		case 'min':
		case 'minute': return [ 'H:i',  60 ];

		case 'h':
		case 'hour'	 : return [ 'H',  3600 ];

		case 'd':
		case 'day'	 : return [ '',  86400 ];

	}

}


function timedhash_verify( $duration, $code='' ){

	$h = trim($_REQUEST['h']);

	if( $h and in_array( timedhash_generate_now($duration, $code), str_split($h, 20) ) ){
		return true;
	
	} else {
		return false;
	}

}


function timedhash_trailer(){
	return 	__FUNCTION__; //.$_SERVER['SERVER_ADDR'];
}


function timedhash_encode( $date, $code ){
	return substr( md5( $date .$code. timedhash_trailer() ) , 0 , 20 );
}











